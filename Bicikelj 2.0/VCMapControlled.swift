//
//  VCMapControlled.swift
//  Bicikelj 2.0
//
//  Created by Tomaz Golob on 16/09/16.
//  Copyright © 2016 Tomaz Golob. All rights reserved.
//

import Foundation
import MapKit

extension ViewController: MKMapViewDelegate {
    
    // Updates when shown on the map
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is Station) {
            return nil
        }
        
        let reuseId = "test"
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = false
            
            (annotation as! Station).nmLbl = UILabel(frame: CGRect(x: -1, y: 0, width: 36, height: 36))
            (annotation as! Station).nmLbl.tag = 42    //set tag on it so we can easily find it later
            (annotation as! Station).nmLbl.textColor = UIColor.white
            (annotation as! Station).nmLbl.font = UIFont(name: "Montserrat-SemiBold", size: 18)
            (annotation as! Station).nmLbl.textAlignment = NSTextAlignment.center
            (annotation as! Station).nmLbl.text = String((annotation as! Station).available)
            
            
            var startTime: CFAbsoluteTime!
            startTime = CFAbsoluteTimeGetCurrent()
            (annotation as! Station).updates = startTime
            
            anView!.addSubview((annotation as! Station).nmLbl)
        }
        else {
            anView!.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! Station
        anView!.image = UIImage(named:cpa.imageName)
        
        
        return anView
    }
    
    
    //func selected
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        self.stationHide()
    }
    
    // The custom pin was selected (no callout)
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        let subText = (view.annotation?.subtitle)!
        var sub = subText?.components(separatedBy: " ")
        let bikes: Int = Int(sub![1])!
        let locks: Int = Int(sub![3])!
        //self.name = nameLabel.text!
        
        self.updateGroup()
        self.showStationDetails(bike: bikes, lock: locks, favorite: false, name: (view.annotation!.title)!!, refreshed: (view.annotation as! Station).updates)
        
    }
    
}
