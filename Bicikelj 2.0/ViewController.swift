//
//  ViewController.swift
//  Bicikelj 2.0
//
//  Created by Tomaz Golob on 15/09/16.
//  Copyright © 2016 Tomaz Golob. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import CoreMotion
import UserNotifications

import SWXMLHash
import Alamofire


class ViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    let initialLocation = CLLocation(latitude: 46.0524, longitude: 14.503)
    
    var bikes: Int = Int()
    var locks: Int = Int()
    var number: Int = Int()
    var name: String = String()
    
    var sBL: Bool = Bool()
    
    var stations = [Station]()
    
    var bikedH: Int = Int()
    var bikedM: Int = Int()
    
    var lastTrigger: Date = Date()
    var lastTriggerTime: Int = Int()
    
    var biking: Bool = Bool()
    var bikeStart: Date = Date()
    var bikeStop: Date = Date()
    var lastLocation: CLLocation = CLLocation()
    
    var count: Int = Int()
    
    var locInUse: Bool = Bool()
    var locAlways: Bool = Bool()
    
    @IBOutlet weak var showBikeLock: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bikesNum: UILabel!
    @IBOutlet weak var locksNum: UILabel!
    @IBOutlet weak var stationDetails: StationDetails!
    
    
    @IBOutlet weak var stationConstraintY: NSLayoutConstraint!
    @IBOutlet weak var stationConstraintX: NSLayoutConstraint!
    //@IBOutlet weak var labelText: UILabel!
    
    var timer: Timer?
    
    //let activityManager = CMMotionActivityManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        count = 0
        
        self.hideKeyboardWhenTappedAround()
        sBL = true
        biking = false
        //labelText.text = "Activity text"
        self.stationConstraintX.constant = 0 //self.stationDetails.frame.width
        
        self.stationConstraintY.constant = -self.stationDetails.frame.height
        
        
        bikes = 0
        locks = 0
        number = 0
        name = "Izberite postajo"
        
        //centerMapOnLocation(location: initialLocation, radius: 1500)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestAlwaysAuthorization()
        if inLjubljana(location: locationManager.location!) {
            centerMapOnLocation(location: locationManager.location!, radius: 2000.0)
        }
        else {
            centerMapOnLocation(location: initialLocation, radius: 5000)
        }
        
        mapView.delegate = self
        
        /*if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdates(to: OperationQueue.main) { data in
                if let data = data {
                    self.bikeTracking(data: data)
                    /*DispatchQueue.global(qos: .background).async {
                        print("This is run on the background queue")
                        DispatchQueue.main.async {
                            self.bikeTracking(data: data)
                        }
                    }*/
                }
            }
        }
 
        registerLocal()
        */
        
        
        if mapView.annotations.count == 0{
            loadInitialData()
        }

    }
    
    /*
    func bikeTracking(data: CMMotionActivity) {
        
        let date = Date()
        let calendar = Calendar.current
        self.bikedH = calendar.component(.hour, from: date)
        self.bikedM = calendar.component(.minute, from: date)
        print(self.bikedH, ":", self.bikedM, ":", calendar.component(.second, from: date))
        self.count = self.count+1
        print(self.count)
        if data.walking == true {
            //self.labelText.text = "Cycling"
            if self.locAlways {
                if !self.biking {
                    //if self.isNearby(loc: self.locationManager.location!){
                        self.biking = true
                        self.bikeStart = Date.init()
                        self.scheduleLocal(waitBeforeNotification: 0.2, message: "Začetek vožnje")
                        
                        //let dateC = Date()
                        let calendarC = Calendar.current
                    let min = calendar.component(.minute, from: date)
                    if (calendarC.component(.minute, from: date) - calendarC.component(.minute, from: self.lastTrigger)) > self.lastTriggerTime{
                        self.scheduleLocal(waitBeforeNotification: 30, message: "30 minut uporabe.")
                        self.scheduleLocal(waitBeforeNotification: 75, message: "Ne pozabite vrniti kolesa.")
                            self.lastTriggerTime = 10
                            self.lastTrigger = date
                        }
                    //}
                }
                
                self.lastLocation = self.locationManager.location!
            }
            else {
                if !self.biking {
                    self.biking = true
                    self.bikeStart = Date.init()
                    self.scheduleLocal(waitBeforeNotification: 0.2, message: "Začetek vožnje")
                    
                    let dateC = Date()
                    let calendarC = Calendar.current
                    if (calendarC.component(.minute, from: dateC) - calendarC.component(.minute, from: self.lastTrigger)) < self.lastTriggerTime {
                        self.scheduleLocal(waitBeforeNotification: 10, message: "Ne pozabite vrniti kolesa.")
                        self.lastTriggerTime = 10
                        self.lastTrigger = dateC
                    }
                }
                
            }
            self.bikeStop = Date.init()
        }
        else {
            //self.labelText.text = "Other"
            let date = Date()
            let calendar = Calendar.current
            if (calendar.component(.minute, from: date) - calendar.component(.minute, from: self.bikeStop)) < 5 {
                if self.locAlways {
                    if self.isNearby(loc: self.lastLocation){
                       // UIApplication.shared.cancelLocalNotification(<#T##notification: UILocalNotification##UILocalNotification#>)
                        
                        self.biking = false
                        self.bikeStop = Date()
                        self.bikeStart = Date()
                        self.lastLocation = CLLocation()
                    }
                }
            }
        }

    }
    
    func scheduleLocal(waitBeforeNotification: Double, message: String) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            let content = UNMutableNotificationContent()
            content.title = "Bicikelj obvestilo"
            content.body = message
            content.categoryIdentifier = "alert"
            //content.userInfo = ["customData": "fizzbuzz"]
            //content.sound = UNNotificationSound.default()
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: waitBeforeNotification * 60, repeats: false)
            print(UUID().uuidString)
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            center.add(request)
            
        } else {
            
            let content = UILocalNotification()
            content.alertTitle = "Bicikelj obvestilo"
            content.alertBody = message
            content.category = "alert"
            
            
        }
        
    }
    
    //local notifications permissions
    func registerLocal() {
        //let center = UNUserNotificationCenter.
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                if granted {
                    print("Yay!")
                } else {
                    print("D'oh")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    */
    func showStationDetails(bike: Int, lock: Int, favorite: Bool, name: String, refreshed: CFAbsoluteTime){
        bikesNum.text = String(bike)
        locksNum.text = String(lock)
        print(self.stationDetails.frame.origin)
        nameLabel.text = name
        
        self.stationConstraintY.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            }, completion: { finished in
        })
    }
    
    
    func stationHide(){
        self.stationConstraintY.constant = -self.stationDetails.frame.height
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            }, completion: { finished in
        })
    }
    
    func loadInitialData() {
        mapView.removeAnnotations(mapView.annotations)
        enum JSONError: String {
            case NoData = "ERROR: no data"
            case ConversionFailed = "ERROR: conversion from JSON failed"
        }
        
        let urlPath = "http://www.bicikelj.si/service/carto"
        let request = NSURL(string: urlPath)!
        let session = URLSession.shared
        let task = session.dataTask(with: request as URL, completionHandler: {
            (data, response, error) in
            
            if data == nil {
                print("dataTaskWithRequest \(error)")
                return
            }
            
            let xml = SWXMLHash.parse(data!)
            var stations = [[String: String?]]()
            
            for elem in xml["carto"]["markers"].children {
                
                if let element = elem.element {
                    let stationDict = [
                        "address" : element.attributes["address"],
                        "number" : element.attributes["number"],
                        "lng" : element.attributes["lng"],
                        "lat" : element.attributes["lat"],
                        "open" : element.attributes["open"],
                        "name" : element.attributes["name"]
                    ]
                    
                    if let station = Station.fromDict(stationDict) {
                        self.stations.append(station)
                    }
                    
                    stations.append(stationDict)
                }
            }
            self.updateStations()
            
        })
        task.resume()
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(ViewController.updateGroup), userInfo: nil, repeats: true)
        
    }
    
    
    func updateGroup() {
        let defaults: UserDefaults = UserDefaults(suiteName: "group.BicikeljGroup")!
        defaults.setValue(bikes, forKey: "bikes")
        defaults.setValue(locks, forKey: "locks")
        defaults.setValue(number, forKey: "number")
        defaults.setValue(name, forKey: "name")
        defaults.synchronize()
    }
    
    func updateAnnotations() {
        for stat in mapView.annotations {
            let av = mapView.view(for: stat)
            var num = 0
            if self.sBL {
                num = (stat as! Station).available
                (stat as! Station).nmLbl.text = String((stat as! Station).available)
            }
            else {
                num = (stat as! Station).free
                (stat as! Station).nmLbl.text = String((stat as! Station).free)
            }
            if num > 6 {
                av?.image = UIImage(named: "pinGreen")
            }
            else if num >= 1 {
                av?.image = UIImage(named: "pinOrange")
            }
            else {
                av?.image = UIImage(named: "pinRed")
            }
        }
    }
    
    func updateStations() {
        for stat in stations {
            let url = "http://www.bicikelj.si/service/stationdetails/ljubljana/\(stat.number)"
            let ses = URLSession.shared
            let request = NSURL(string: url)!
            let task = ses.dataTask(with: request as URL, completionHandler: {
                (data, response, error) in
                if data == nil {
                    print("dataTaskWithRequest \(error)")
                    return
                }
                var xml1 = SWXMLHash.parse(data!)
                
                if Int((response?.description.components(separatedBy: "code: ").last?.components(separatedBy: ",").first)!)! == 404 {
                    
                    let taskAgain = ses.dataTask(with: request as URL, completionHandler: {
                        (dataAgain, responseAgain, errorAgain) in
                        if dataAgain == nil {
                            print("dataTaskWithRequest \(errorAgain)")
                            return
                        }
                        xml1 = SWXMLHash.parse(dataAgain!)
                        
                        
 
                    })
                    taskAgain.resume()
                }
                
                
                if xml1["station"]["free"].element?.text == nil {
                    print(stat.number)
                    print("Error getting station data ")
                    return
                }
                //print(xml1["station"].element?.text)
                stat.free = Int(xml1["station"]["free"].element?.text ?? "-1")!
                stat.available = Int(xml1["station"]["available"].element?.text ?? "-1")!
                //stat.available = x
                stat.total = Int(xml1["station"]["total"].element?.text ?? "-1")!
                
                
                var num = 0
                if self.sBL {
                    num = stat.available
                    stat.nmLbl.text = String(stat.available)
                }
                else {
                    num = stat.free
                    stat.nmLbl.text = String(stat.free)
                }
                if num > 6 {
                    stat.imageName = "pinGreen"
                }
                else if num >= 1 {
                    stat.imageName = "pinOrange"
                }
                else {
                    stat.imageName = "pinRed"
                }
                
                //stat.imageName = "Annotation-small"
                
                
                self.bikes = stat.available
                self.locks = stat.free
                self.number = stat.number
                self.name = stat.locationName
                
                
                DispatchQueue.main.async(){
                    self.mapView.addAnnotation(stat)
                }
            })
            task.resume()
        }
    }
    
    @IBAction func sBLPressed(_ sender: AnyObject) {
        
        if sBL {
            sBL = false
            showBikeLock.setImage(#imageLiteral(resourceName: "showLock"), for: UIControlState.normal)
        }
        else {
            sBL = true
            showBikeLock.setImage(#imageLiteral(resourceName: "showBike"), for: UIControlState.normal)
        }
        
        for stat in stations {
            var num = 0
            if self.sBL {
                num = stat.available
                stat.nmLbl.text = String(stat.available)
            }
            else {
                num = stat.free
                stat.nmLbl.text = String(stat.free)
            }
            if num > 6 {
                stat.imageName = "pinGreen"
            }
            else if num >= 1 {
                stat.imageName = "pinOrange"
            }
            else {
                stat.imageName = "pinRed"
            }
            
        }
        updateAnnotations()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            locInUse = true
            locAlways = false
            break
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
            locInUse = false
            locAlways = true
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.last != nil {
            print(locationManager.allowsBackgroundLocationUpdates)
            if locationManager.allowsBackgroundLocationUpdates != true {
                locationManager.allowsBackgroundLocationUpdates = true
            }
        }
        
        /*
        if(CMMotionActivityManager.isActivityAvailable()){
            self.activityManager.startActivityUpdates(to: OperationQueue.main) { data in
                if let data = data {
                    self.bikeTracking(data: data)
                    /*DispatchQueue.global(qos: .background).async {
                        print("This is run on the background queue")
                        DispatchQueue.main.async {
                            self.count = self.count+1
                            self.labelText.text = "\(self.count)"
                            for location in locations {
                                print("Location: ", location.coordinate, "  ", location.timestamp)
                            }
                            self.bikeTracking(data: data)
                        }
                    }*/
                }
            }
        }
        */
    }
    
    /*
    //check if it's near the station
    func isNearby(loc: CLLocation) -> Bool {
        let closer = 0.01
        for stat in stations {
            let boolClose = fabs(loc.coordinate.latitude - stat.lat) <= closer && fabs(loc.coordinate.longitude - stat.lng) <= closer
            if boolClose {
                return true
            }
        }
        return false
    }
    */
    
    func inLjubljana (location: CLLocation) -> Bool {
        if location.coordinate.latitude > 46.11 && location.coordinate.longitude < 46.0 {
            return false
        }
        if location.coordinate.longitude > 14.6 && location.coordinate.longitude < 14.4 {
            return false
        }
        return true
    }
    
    func centerMapOnLocation(location: CLLocation, radius: Double) {
        let cRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, radius, radius)
        mapView.setRegion(cRegion, animated: false)
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

