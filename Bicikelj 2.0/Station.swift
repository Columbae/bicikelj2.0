//
//  Station.swift
//  Bicikelj 2.0
//
//  Created by Tomaz Golob on 16/09/16.
//  Copyright © 2016 Tomaz Golob. All rights reserved.
//

import Foundation
import MapKit
import AddressBook
import Contacts

class Station: NSObject, MKAnnotation {
    var title: String?
    let locationName: String
    var free: Int
    var available: Int
    var total: Int
    let number: Int
    let open: Bool
    let coordinate: CLLocationCoordinate2D
    var nmLbl: UILabel = UILabel()
    let lat: Double
    let lng: Double
    
    var imageName: String!
    var updates: CFAbsoluteTime
    
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D, number: Int, open: Bool, lat: Double, lng: Double) {
        self.title = title
        self.locationName = locationName
        self.free = 0
        self.coordinate = coordinate
        self.number = number
        self.open = open
        self.available = 0
        self.total = 0
        self.updates = 0
        self.lat = lat
        self.lng = lng
        super.init()
    }
    
    var subtitle: String? {
        return "B \(available) L \(free)"
    }
    
    // annotation callout info button opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        let addressDictionary = [String(CNPostalAddressStreetKey): subtitle!]
        let placemark = MKPlacemark.init(coordinate: coordinate, addressDictionary: addressDictionary)
        
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        //print(title)
        
        return mapItem
    }
    
    class func fromDict(_ dict : Dictionary<String,String?>) -> Station? {
        
        var name: String
        var address: String
        var number: Int
        var lat: Double
        var lng: Double
        var open: Bool
        
        
        if let nameOrNil = dict["name"] {
            name = nameOrNil!
        } else {
            name = ""
        }
        if let adressOrNil = dict["address"] {
            address = adressOrNil!
        } else {
            address = ""
        }
        if let numberOrNil = Int(dict["number"]!!) {
            number = numberOrNil
        } else {
            number = 0
        }
        if let latOrNil = Double(dict["lat"]!!) {
            lat = latOrNil
        } else {
            lat = 0
        }
        if let lngOrNil = Double(dict["lng"]!!) {
            lng = lngOrNil
        } else {
            lng = 0
        }
        if let openOrNil = dict["open"] {
            let openTmp = openOrNil!
            open = openTmp.stringToBool()!
        } else {
            open = false
        }
        
        
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        
        
        return Station(title: name, locationName: address, coordinate: coordinate, number: number, open: open, lat: lat, lng: lng)
    }
}

extension String {
    func stringToBool() -> Bool? {
        if self == "1" {
            return true
        }
        return false
    }
}
