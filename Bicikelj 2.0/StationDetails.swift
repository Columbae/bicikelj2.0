//
//  StationDetails.swift
//  Bicikelj 2.0
//
//  Created by Tomaz Golob on 16/09/16.
//  Copyright © 2016 Tomaz Golob. All rights reserved.
//


import UIKit

class StationDetails: UIView {
    
    //var favoriteButton = UIButton()
    //var shareButton = UIButton()
    var favorite = Bool()
    var bikes = Int()
    var locks = Int()
    
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        /*favoriteButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        favoriteButton.addTarget(self, action: #selector(StationDetails.favoritedTapped(button:)), for: .touchUpInside)
        favoriteButton.backgroundColor = UIColor.red //remove when top is in
        
        addSubview(favoriteButton)
        
        
        shareButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        shareButton.addTarget(self, action: #selector(StationDetails.shareTapped(button:)), for: .touchUpInside)
        shareButton.backgroundColor = UIColor.cyan
        addSubview(shareButton)*/
    }
    
    /*override func layoutSubviews() {
        let buttonSize = 30
        
        var buttonFrame = CGRect(x: 0, y: 0, width: buttonSize, height: buttonSize)
        
        buttonFrame.origin.x = CGFloat(10)
        buttonFrame.origin.y = CGFloat(50)
        favoriteButton.frame = buttonFrame
        
        buttonFrame.origin.x = CGFloat(55)
        shareButton.frame = buttonFrame
        // Offset each button's origin by the length of the button plus spacing.
        
    }*/
    
    override var intrinsicContentSize : CGSize {
        return CGSize(width: 375, height: 240)
    }
    
    /*
    // MARK: Update the entries
    func updateEntries(bike: Int, lock: Int, favorite: Bool, name: String, refreshed: Int){
        
    }
    
    
    // MARK: Button Action
    func favoritedTapped(button: UIButton) {
        print("Button pressed FAVORITE")
    }
    
    func shareTapped(button: UIButton) {
        print("Button pressed SHARE")
    }
 */
    
}
